package com.example.franke.gwyntscorecounter;



import android.content.Context;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class ScoreFragment extends Fragment implements OnClickListener {

    private TextView textScore;
    private ScoreFragmentListener activityCommander;

    public interface ScoreFragmentListener
    {
        void changeScore(View view, int num);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.score_fragment,container,false);

        //set custom font
        textScore = (TextView) view.findViewById(R.id.text_score);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"mason.ttf");
        textScore.setTypeface(typeface);

        //set onclicklistener for buttons
        Button b1 = (Button) view.findViewById(R.id.button_plus_1);
        Button b2 = (Button) view.findViewById(R.id.button_plus_10);
        Button b3 = (Button) view.findViewById(R.id.button_plus_100);
        Button b4 = (Button) view.findViewById(R.id.button_minus_1);
        Button b5 = (Button) view.findViewById(R.id.button_minus_10);
        Button b6 = (Button) view.findViewById(R.id.button_minus_100);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);


        return view;
    }

    //attach to activity
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try
        {
            activityCommander = (ScoreFragmentListener) context;
        }
        catch(ClassCastException ex)
        {
            throw new ClassCastException(context.toString());
        }
    }


    @Override
    public void onClick(View v) {

        //add some vibration feedback
        Vibrator vibe = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(66);

        switch(v.getId())
        {
            //check which button was pressed and then call changeScore
            //passing view to get known from which fragment was metohd called
            case R.id.button_plus_1:
                activityCommander.changeScore(this.getView(), 1);
                break;
            case R.id.button_plus_10:
                activityCommander.changeScore(this.getView(), 10);
                break;
            case R.id.button_plus_100:
                activityCommander.changeScore(this.getView(), 100);
                break;
            case R.id.button_minus_1:
                activityCommander.changeScore(this.getView(), -1);
                break;
            case R.id.button_minus_10:
                activityCommander.changeScore(this.getView(), -10);
                break;
            case R.id.button_minus_100:
                activityCommander.changeScore(this.getView(), -100);
                break;
        }
    }

    public void resetScore()
    {
        textScore.setText("0");
    }

    public void fadeOutText()
    {
         textScore.setTextColor(getActivity().getResources().getColor(R.color.colorScoreTextFade));
    }

    public void fadeInText()
    {
        textScore.setTextColor(getActivity().getResources().getColor(R.color.colorScoreText));
    }
}
