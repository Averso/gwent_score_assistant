package com.example.franke.gwyntscorecounter;


import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import java.util.ArrayList;

//package local
class Player {

    private Integer score;
    private Integer looses;
    private boolean leaderAbilityAvalible;
    private ArrayList<Gem> gems;
    private ImageView abilityImage;
    private Drawable abilityDrawableOn;
    private Drawable abilityDrawableOff;
    private boolean passed;

    //constructor
    Player(Gem gem1, Gem gem2,ImageView abilityImage, Drawable abOn, Drawable abOff)
    {
        this.score=0;
        this.looses=0;
        this.leaderAbilityAvalible=true;
        this.gems = new ArrayList<>();
        this.gems.add(gem1);
        this.gems.add(gem2);
        this.abilityImage=abilityImage;
        this.abilityDrawableOn=abOn;
        this.abilityDrawableOff=abOff;
        this.passed=false;

    }

    Integer getScore()
    {
        return this.score;
    }

    void setScore(Integer score){
        if(!passed)
         this.score=score;
    }

    Integer getLooses()
    {
        return this.looses;
    }

    Boolean getPassed()
    {
        return this.passed;
    }

    void setPassed(boolean passed)
    {
     this.passed=passed;
    }


    void useAbility()
    {
        //if there is possibility to use ability and player hasn passed
        if(leaderAbilityAvalible && !passed)
        {
            leaderAbilityAvalible=false;
            abilityImage.setImageDrawable(abilityDrawableOff);
        }
    }

    void sumUpRound(boolean lost)
    {
        //if player has lost this round
        if(lost)
        {
            //disable one gem and increment looses counter
            gems.get(looses).turnOff();
            looses++;
        }
    }

    void nextRoundReset()
    {

        score=0;
        passed=false;
        leaderAbilityAvalible=true;
        abilityImage.setImageDrawable(abilityDrawableOn);

    }

    void reset()
    {

        passed=false;
        looses=0;
        leaderAbilityAvalible=true;
        abilityImage.setImageDrawable(abilityDrawableOn);
        score=0;

        //enable all gems
        for (Gem gem : gems)
        {
            gem.turnOn();
        }
    }




}
