package com.example.franke.gwyntscorecounter;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Franke on 2016-09-27.
 */

//TODO: maybe merge into one class with ability image, drawable wolud be passed
public class Gem extends ImageView {

    private final Context context;

    //constructor
    public Gem(Context context)
    {
        super(context);
        this.context=context;

        setImageDrawable(context.getResources().getDrawable(R.drawable.ic_gem_on,null));

    }

    //two constructors needed for proper work in layout
    public Gem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        init(context);
    }

    public Gem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context=context;
        init(context);
    }

    //TODO: check this out
    private void init(Context context) {
        //do stuff that was in your original constructor...
    }

    public void turnOff()
    {
        setImageDrawable(context.getResources().getDrawable(R.drawable.ic_gem_off,null));
    }

    public void turnOn()
    {
        setImageDrawable(context.getResources().getDrawable(R.drawable.ic_gem_on,null));
    }




}
