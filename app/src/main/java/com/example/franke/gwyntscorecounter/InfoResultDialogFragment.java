package com.example.franke.gwyntscorecounter;

import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


public class InfoResultDialogFragment extends android.support.v4.app.DialogFragment {


    private String text;
    private long animDuration;

    public InfoResultDialogFragment(){}

    public static InfoResultDialogFragment newInstance(String message, long animDuration) {
        InfoResultDialogFragment f = new InfoResultDialogFragment();


        Bundle args = new Bundle();
        args.putString("message", message);
        args.putLong("anim_duration",animDuration);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        text = getArguments().getString("message");
        animDuration = getArguments().getLong("anim_duration");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

      //  Dialog dialog  = new Dialog(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);

        Dialog dialog  = new Dialog(getActivity(),R.style.EndRooundDialog);

        dialog.setContentView(R.layout.info_result_fragment);

        //set custom typeface for textView
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"din1451alt_G.ttf");

        //set textVie
        TextView textView = (TextView) dialog.findViewById(R.id.text_end_round);
        textView.setTypeface(typeface);
        textView.setText(this.text);

        onViewCreated(dialog.findViewById(R.id.layout_dialog_fragment),savedInstanceState);
        return dialog;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.move_slighty_down);
        animation.setDuration(animDuration);
        view.startAnimation(animation);
    }

}
