package com.example.franke.gwyntscorecounter;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements ScoreFragment.ScoreFragmentListener {

    //durations for countdowntimer and animations
    private static final long END_ROUND_DIALOG_DURATION = 2000; //milisecs
    private static final long NEW_GAME_DIALOG_DURATION = 1000;

    //music related variables
    private int[] musicArray = {R.raw.music_1, R.raw.music_2, R.raw.music_3, R.raw.music_4, R.raw.music_5};
    private MediaPlayer mediaPlayer;
    private int compleated = 0;

    private Player playerLeft;
    private Player playerRight;

    private CheckBox checkBoxMusic;
    private Button buttonPassLeft;
    private Button buttonPassRight;

    private CheckBox checkBoxNilfgaardLeft;
    private CheckBox checkBoxNilfgaardRight;

    private boolean lockNilfgaarCheckbox;

    private ScoreFragment fragmentScoreLeft;
    private ScoreFragment fragmentScoreRight;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //flag to prevent changing nilfgaard checkbox while playing
        lockNilfgaarCheckbox=false;
        Resources r = getResources();

        //get layout views
       // buttonNextRound = (Button) findViewById(R.id.button_next_round);
        buttonPassLeft = (Button) findViewById(R.id.button_pass_left);
        buttonPassRight = (Button) findViewById(R.id.button_pass_right);
        checkBoxNilfgaardLeft = (CheckBox) findViewById(R.id.checkbox_nilf_left);
        checkBoxNilfgaardRight = (CheckBox) findViewById(R.id.checkbox_nilf_right);
        checkBoxMusic = (CheckBox) findViewById(R.id.button_music);

        //get custom typeface
        Typeface typeface = Typeface.createFromAsset(getAssets(),"mason.ttf");



        //add listeners to ability buttons
        ImageView abilityLeft = (ImageView) findViewById(R.id.image_ability_left);
        ImageView abilityRight = (ImageView) findViewById(R.id.image_ability_right);

        abilityLeft.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                playerLeft.useAbility();
                lockNilfgaardCheckBoxes();
            }
        });
        abilityRight.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                playerRight.useAbility();
                lockNilfgaardCheckBoxes();
            }
        });


        //set players
        playerLeft = new Player((Gem)findViewById(R.id.gem_left_1),(Gem)findViewById(R.id.gem_left_2),
                abilityLeft,r.getDrawable(R.drawable.leader_ability_left_on,null),
                                                r.getDrawable(R.drawable.leader_ability_left_off,null));

        playerRight = new Player((Gem) findViewById(R.id.gem_right_1), (Gem) findViewById(R.id.gem_right_2),
                abilityRight,r.getDrawable(R.drawable.leader_ability_right_on,null),
                                                r.getDrawable(R.drawable.leader_ability_right_off,null));



        // set custom font for next round button
       // buttonNextRound.setTypeface(typeface);

        // set custom font for pass buttons
        buttonPassLeft.setTypeface(typeface);
        buttonPassRight.setTypeface(typeface);

        //set custom font for checkboxes
        checkBoxNilfgaardLeft.setTypeface(typeface);
        checkBoxNilfgaardRight.setTypeface(typeface);


        //add listener to checkbox for statring/pausing music
        checkBoxMusic.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                CheckBox self = (CheckBox)v;

                //turn on pause music
                if(self.isChecked())
                {
                    mediaPlayer.start();
                }
                else
                {
                    mediaPlayer.pause();
                }


            }
        });

        //add listeners to nilfgard checkboxes to prevent enabling two at once
        checkBoxNilfgaardLeft.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(checkBoxNilfgaardRight.isChecked())
                {
                    checkBoxNilfgaardLeft.setChecked(false);
                }
            }
        });

        checkBoxNilfgaardRight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(checkBoxNilfgaardLeft.isChecked())
                {
                    checkBoxNilfgaardRight.setChecked(false);
                }
            }
        });


        //get score fragments
        fragmentScoreLeft = (ScoreFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_score_left);
        fragmentScoreRight = (ScoreFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_score_right);


        //add listeners to pass buttons to manage winning loosing

        buttonPassLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if left player hasn't passed yet
                if(!playerLeft.getPassed())
                {
                    playerLeft.setPassed(true);
                    v.setBackground(getResources().getDrawable(R.drawable.btn_pass_on));

                    //fade score text
                    fragmentScoreLeft.fadeOutText();

                    //if other player has passed
                    if(playerRight.getPassed())
                    {
                        sumUpRound();
                    }
                }
                lockNilfgaardCheckBoxes();

            }
        });

        buttonPassRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if right player hasn't passed yet
                if(!playerRight.getPassed())
                {
                    playerRight.setPassed(true);
                    v.setBackground(getResources().getDrawable(R.drawable.btn_pass_on));

                    //fade score text
                    fragmentScoreRight.fadeOutText();

                    //if other player has passed
                    if(playerLeft.getPassed())
                    {
                        sumUpRound();
                    }
                }
                lockNilfgaardCheckBoxes();

            }
        });



    }

    @Override
    //view - which fragment has called this method, num - numer to add/substract fro mscore
    public void changeScore(View view, int num)
    {

        //TODO: Make it better
        // if player has passed, dont change score


        if(view.getTag().equals(getResources().getString(R.string.tag_fragment_left)) && playerLeft.getPassed())
            return;
        if(view.getTag().equals(getResources().getString(R.string.tag_fragment_right)) && playerRight.getPassed())
            return;


        //get textview from fragment
        TextView textScore = (TextView) view.findViewById(R.id.text_score);
        Integer score = Integer.parseInt(textScore.getText().toString());

        //check if we dont get below 0 and above 999
        if (score+num >= 0 && score+num <=999)
        {
            //if not, do the addition
            score+=num;
            textScore.setText(score.toString());

            //set score in player class
            if(view.getTag().equals(getResources().getString(R.string.tag_fragment_left)))
                playerLeft.setScore(score);
            else
                playerRight.setScore(score);
        }

        lockNilfgaardCheckBoxes();

    }

    @Override
    protected void onStart() {
        super.onStart();

        mediaPlayer = MediaPlayer.create(this,musicArray[0]);

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                compleated++;
                mp.reset();
                if(compleated<musicArray.length)
                {
                    try
                    {
                        AssetFileDescriptor afd = getResources().openRawResourceFd(musicArray[compleated]);
                        if(afd!=null)
                        {
                            mp.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
                            afd.close();
                            mp.prepare();
                            mp.start();
                        }
                    }
                    catch (Exception ex)
                    {
                        //TODO: its bad
                    }
                }
                else if (compleated>=musicArray.length)
                {
                    compleated=0;
                    try
                    {
                        AssetFileDescriptor afd = getResources().openRawResourceFd(musicArray[compleated]);
                        if (afd != null)
                        {
                            mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                            afd.close();
                            mp.prepare();
                            mp.start();
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
                else
                {
                    compleated=0;
                    mp.release();
                    mp = null;
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(checkBoxMusic.isChecked() && mediaPlayer!=null)
            mediaPlayer.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        checkBoxMusic.setChecked(false);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
        mediaPlayer=null;
    }

    //for refresh button
    public void resetNewGame(View view)
    {
        //call
       // resetAll();
        newGame();
    }

    private void resetScores()    {
        //reset scores
        fragmentScoreLeft.resetScore();
        fragmentScoreRight.resetScore();
    }


    private void resetAll()
    {
        //reset players
        playerLeft.reset();
        playerRight.reset();

        //reset scores
        resetScores();

        //change nilfgaard checkboxes to false
        checkBoxNilfgaardLeft.setChecked(false);
        checkBoxNilfgaardRight.setChecked(false);

        //starting next round after ending the game - change flag
       // if(finishedGame)
       //     finishedGame=false;

        //switch down pass buttons
        buttonPassLeft.setBackground(getResources().getDrawable(R.drawable.btn_pass_off));
        buttonPassRight.setBackground(getResources().getDrawable(R.drawable.btn_pass_off));

        //enable nilfgaard checkboxes
        lockNilfgaarCheckbox=false;
        checkBoxNilfgaardLeft.setEnabled(true);
        checkBoxNilfgaardRight.setEnabled(true);

        //change text color
        fragmentScoreLeft.fadeInText();
        fragmentScoreRight.fadeInText();

    }

    private void sumUpRound()
    {
        WinState winState;
        boolean endOfGame = false;

        Log.d("player left score", playerLeft.getScore().toString());
        Log.d("player right score", playerRight.getScore().toString());


        if(playerLeft.getScore()==playerRight.getScore()) //draw
        {


            //check if one of players have nilfgaard deck
            if(checkBoxNilfgaardLeft.isChecked()) //left wins
            {
                winState=WinState.LEFT;
                playerLeft.sumUpRound(false);
                playerRight.sumUpRound(true);

            }
            else if(checkBoxNilfgaardRight.isChecked()) //right wins
            {

                winState=WinState.RIGHT;
                playerLeft.sumUpRound(true);
                playerRight.sumUpRound(false);

            }
            else //no one has nilfgaard deck = draw
            {
                winState=WinState.DRAW;
                playerLeft.sumUpRound(true);
                playerRight.sumUpRound(true);

            }

        }
        else if(playerLeft.getScore()>playerRight.getScore()) //left player wins
        {
            winState=WinState.LEFT;
            playerLeft.sumUpRound(false);
            playerRight.sumUpRound(true);


        }
        else //right player wins
        {
            winState=WinState.RIGHT;
            playerLeft.sumUpRound(true);
            playerRight.sumUpRound(false);
        }

        //check if this is end of game
        if(playerLeft.getLooses()==2 || playerRight.getLooses()==2)
            endOfGame=true;


        //do actions depending on who wins and if its end of the game
        switch (winState)
        {
            case DRAW:
                afterRoundAction(getResources().getString(R.string.info_draw),endOfGame);
                break;
            case LEFT:
                if(endOfGame)
                    afterRoundAction(getResources().getString(R.string.info_player_left_won_game),true);
                else
                    afterRoundAction(getResources().getString(R.string.info_player_left_won_round),false);
                break;
            case RIGHT:
                if(endOfGame)
                    afterRoundAction(getResources().getString(R.string.info_player_right_won_game),true);
                else
                    afterRoundAction(getResources().getString(R.string.info_player_right_won_round),false);
                break;
        }

    }

    private void afterRoundAction(String message,final boolean itsEndOfGame)
    {
        //lock window to prevent messing up with buttons by user
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        //make end round dialog fragment
        final InfoResultDialogFragment infoResultDialogFragment = InfoResultDialogFragment.newInstance(message,END_ROUND_DIALOG_DURATION);
        infoResultDialogFragment.show(getSupportFragmentManager(),"");


        //start timer - after finish we close dialog and reset stats depending on if its end of round or game
        CountDownTimer cdt = new CountDownTimer(END_ROUND_DIALOG_DURATION,1000)
        {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                //close dialog
                infoResultDialogFragment.dismiss();

                if(itsEndOfGame)
                {
                    //end of game - just reset all
                   // resetAll();
                    newGame();
                }
                else
                {
                    //reset players atributes
                    playerLeft.nextRoundReset();
                    playerRight.nextRoundReset();

                    //reset scores in fragments
                    resetScores();

                    //change pass button images
                    buttonPassLeft.setBackground(getResources().getDrawable(R.drawable.btn_pass_off));
                    buttonPassRight.setBackground(getResources().getDrawable(R.drawable.btn_pass_off));

                    //change text color
                    fragmentScoreLeft.fadeInText();
                    fragmentScoreRight.fadeInText();

                }


                //release not-touch flag
                MainActivity.this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        };
        cdt.start();
    }

    private void newGame()
    {
        //lock window to prevent messing up with buttons by user
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        //make new game dialog
        final InfoResultDialogFragment infoResultDialogFragment = InfoResultDialogFragment.newInstance(getResources().getString(R.string.info_new_game),NEW_GAME_DIALOG_DURATION);
        infoResultDialogFragment.show(getSupportFragmentManager(), "");


        //start timer - after finish we close dialog and reset stats depending on if its end of round or game
        CountDownTimer cdt = new CountDownTimer(NEW_GAME_DIALOG_DURATION,1)
        {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                //TODO: add if not null

                resetAll();
                //close dialog
                infoResultDialogFragment.dismiss();
                //release not-touch flag
                MainActivity.this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        };
        cdt.start();
    }

    private void lockNilfgaardCheckBoxes()
    {
        //lock nilfgaar checkboxes
        if(!lockNilfgaarCheckbox)
        {
            lockNilfgaarCheckbox=true;
            checkBoxNilfgaardLeft.setEnabled(false);
            checkBoxNilfgaardRight.setEnabled(false);
        }
    }

    private enum WinState{
        LEFT,
        RIGHT,
        DRAW
    }


}
